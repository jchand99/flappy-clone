using System;
using System.Diagnostics;
using Magma.Maths;
using Magma.OpenGL;
using Magma.OpenGL.GLFW;

namespace TestMe
{
    public class Application
    {
        private IntPtr _Window;
        private Level _Level;

        public Application()
        {
            Init();
        }

        private void Init()
        {
            Glfw.Init();

            Glfw.WindowHint(WindowHint.ContextVersionMajor, 4);
            Glfw.WindowHint(WindowHint.ContextVersionMinor, 5);
            Glfw.WindowHint(WindowHint.OpenGLProfile, (int) OpenGLProfile.Core);

            _Window = Glfw.CreateWindow(1280, 720, "Flappy", IntPtr.Zero, IntPtr.Zero);
            if(_Window == IntPtr.Zero)
            {
                Glfw.GetError(out var err);
                Console.WriteLine(err);
                Glfw.Terminate();
                Environment.Exit(-1);
            }

            Glfw.MakeContextCurrent(_Window);
            Gl.Enable(EnableCap.DepthTest);
            Gl.Enable(EnableCap.Blend);
            Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            Console.WriteLine($"OpenGL: {Gl.GetString(StringName.Vendor)}");

            Input.SetCallback(_Window);

            _Level = new Level();
        }

        public void Run()
        {
            int frames = 0;
            double timer = Glfw.GetTime();
            double delta = Glfw.GetTime();
            while(!Glfw.WindowShouldClose(_Window))
            {
                if(Glfw.GetTime() - timer >= 1.0)
                {
                    timer = Glfw.GetTime();
                    Console.WriteLine($"fps: {frames}");
                    Console.WriteLine($"tps: {1000 / frames}");
                    frames = 0;
                }
                double deltaTime = Glfw.GetTime() - delta;
                delta = Glfw.GetTime();
                Update(deltaTime);
                Render();
                frames++;
            }

            _Level.Dispose();

            Glfw.Terminate();
        }

        private void Update(double deltaTime)
        {
            Glfw.PollEvents();
            _Level.Update(deltaTime);
        }

        private void Render()
        {
            Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            Gl.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);

            _Level.Render();

            Glfw.SwapBuffers(_Window);
        }
    }
}
