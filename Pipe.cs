using Magma.Maths;

namespace TestMe
{
    public class Pipe
    {
        public Vector3f Position { get; set; }
        public static float Width { get; set; } = 1.5f;
        public static float Height { get; set; } = 8.0f;
        public static VertexArray Mesh { get; set; }
        public Matrix4f Model { get; set; }

        public Pipe(float x, float y)
        {
            Position = new Vector3f(x, y, 0.0f);
            Model = Matrix4f.Translate(Position);
        }

        public static void Create()
        {
            float[] vertices =
            {
                0.0f, 0.0f, 0.1f,
                0.0f, Height, 0.1f,
                Width, Height, 0.1f,
                Width, 0.0f, 0.1f,
            };

            uint[] indices =
            {
                0, 1, 2,  // first Triangle
                2, 3, 0   // second Triangle
            };

            float[] texCoords = {
                0.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f
            };

            Mesh = new VertexArray(vertices, indices, texCoords);
        }

        public void Dispose()
        {
            Mesh.Dispose();
        }
    }
}
