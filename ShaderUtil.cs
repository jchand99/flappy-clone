using System;
using Magma.OpenGL;

namespace TestMe
{
    public static class ShaderUtil
    {

        static ShaderUtil() {}

        public static uint Load(string vertPath, string fragPath)
        {
            byte[] vertSource = FileUtil.ReadFile(vertPath);
            byte[] fragSource = FileUtil.ReadFile(fragPath);

            return Compile(vertSource, fragSource);
        }

        public static uint Compile(byte[] vertSource, byte[] fragSource)
        {
            uint vshader = Gl.CreateShader(ShaderType.VertexShader);
            Gl.ShaderSource(vshader, vertSource, null);
            Gl.CompileShader(vshader);
            Gl.GetShaderiv(vshader, ShaderParameter.CompileStatus, out var success);
            if(success < 1)
            {
                Gl.GetShaderInfoLog(vshader, 2048, out int length, out string infoLog);
                Console.WriteLine(infoLog);
            }

            uint fshader = Gl.CreateShader(ShaderType.FragmentShader);
            Gl.ShaderSource(fshader, fragSource, null);
            Gl.CompileShader(fshader);
            Gl.GetShaderiv(fshader, ShaderParameter.CompileStatus, out success);
            if(success < 1)
            {
                Gl.GetShaderInfoLog(fshader, 2048, out int length, out string infoLog);
                Console.WriteLine(infoLog);
            }

            uint program = Gl.CreateProgram();
            Gl.AttachShader(program, vshader);
            Gl.AttachShader(program, fshader);
            Gl.LinkProgram(program);

            Gl.DeleteShader(vshader);
            Gl.DeleteShader(fshader);

            return program;
        }
    }
}
