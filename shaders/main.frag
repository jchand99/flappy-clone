#version 450 core

out vec4 FragColor;

uniform vec2 bird;
uniform sampler2D tex;

in DATA
{
    vec2 tc;
    vec3 position;
} fs_in;

void main()
{
    FragColor = texture(tex, fs_in.tc);
    FragColor *= 3.0 / (length(bird.xy - fs_in.position.xy) + 2.5) + 0.5;
}
