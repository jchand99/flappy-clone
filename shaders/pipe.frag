#version 450 core

out vec4 FragColor;

uniform int top;
uniform vec2 bird;
uniform sampler2D tex;

in DATA
{
    vec2 tc;
    vec3 position;
} fs_in;

vec2 newTexCoords;

void main()
{
    if(top == 1)
    {
        newTexCoords.x = fs_in.tc.x;
        newTexCoords.y = 1.0 - fs_in.tc.y;
    }
    else
    {
        newTexCoords = fs_in.tc;
    }

    FragColor = texture(tex, newTexCoords);
    if(FragColor.w < 1.0)
        discard;

    FragColor *= 3.0 / (length(bird - fs_in.position.xy) + 2.5) + 0.2;
    FragColor.w = 1.0;
}
