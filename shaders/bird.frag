#version 450 core

out vec4 FragColor;

uniform sampler2D tex;

in DATA
{
    vec2 tc;
} fs_in;

void main()
{
    FragColor = texture(tex, fs_in.tc);
}
