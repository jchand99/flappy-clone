using System;
using Magma.OpenGL;

namespace TestMe
{
    public class VertexArray
    {

        private uint[] _VAO, _VBO, _IBO, _TBO;
        private int _Count;

        public VertexArray(int count)
        {
            _Count = count;
            Gl.GenVertexArrays(1, out _VAO);
        }

        public VertexArray(float[] vertices, uint[] indices, float[] textureCoordinates)
        {
            _Count = indices.Length;
            Gl.GenVertexArrays(1, out _VAO);
            Gl.BindVertexArray(_VAO[0]);

            Gl.GenBuffers(1, out _VBO);
            Gl.BindBuffer(BufferTarget.ArrayBuffer, _VBO[0]);
            Gl.BufferData<float>(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
            Gl.VertexAttribPointer(Shader.VERTEX_ATTRIB, 3, VertexAttribPointerType.Float, false, 0, 0);
            Gl.EnableVertexAttribArray(Shader.VERTEX_ATTRIB);

            if(textureCoordinates.Length != 0)
            {
                Gl.GenBuffers(1, out _TBO);
                Gl.BindBuffer(BufferTarget.ArrayBuffer, _TBO[0]);
                Gl.BufferData<float>(BufferTarget.ArrayBuffer, textureCoordinates.Length * sizeof(float), textureCoordinates, BufferUsageHint.StaticDraw);
                Gl.VertexAttribPointer(Shader.TCOORD_ATTRIB, 2, VertexAttribPointerType.Float, false, 0, 0);
                Gl.EnableVertexAttribArray(Shader.TCOORD_ATTRIB);
            }

            Gl.GenBuffers(1, out _IBO);
            Gl.BindBuffer(BufferTarget.ElementArrayBuffer, _IBO[0]);
            Gl.BufferData<uint>(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(uint), indices, BufferUsageHint.StaticDraw);

            Gl.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);
            Gl.BindVertexArray(0);
        }

        public void Bind()
        {
            Gl.BindVertexArray(_VAO[0]);
            if(_IBO != null && _IBO[0] > 0)
                Gl.BindBuffer(BufferTarget.ElementArrayBuffer, _IBO[0]);
        }

        public void Unbind()
        {

            if(_IBO != null && _IBO[0] > 0)
                Gl.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            Gl.BindVertexArray(0);
        }

        public void Draw()
        {
            if(_IBO != null && _IBO[0] > 0)
                Gl.DrawElements(BeginMode.Triangles, _Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            else
                Gl.DrawArrays(BeginMode.Triangles, 0, _Count);
        }

        public void Render()
        {
            Bind();
            Draw();
        }

        public void Dispose()
        {
            Gl.DeleteBuffers(1, _VBO);
            Gl.DeleteBuffers(1, _IBO);
            Gl.DeleteBuffers(1, _TBO);
            Gl.DeleteVertexArrays(1, _VAO);
        }
    }
}
