using System.IO;

namespace TestMe
{
    public static class FileUtil
    {

        static FileUtil() {}

        public static byte[] ReadFile(string filePath)
        {
            if(File.Exists(filePath))
            {
                return File.ReadAllBytes(filePath);
            }
            else
            {
                return new byte[0];
            }
        }
    }
}
