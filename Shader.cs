using System;
using Magma.OpenGL;
using Magma.Maths;
using System.Collections.Generic;

namespace TestMe
{
    public class Shader
    {
        public static uint VERTEX_ATTRIB = 0;
        public static uint TCOORD_ATTRIB = 1;

        private uint _ID;
        private Dictionary<string, int> _LocationCache;
        private bool _Enabled = false;

        public static Shader Background;
        public static Shader Bird;
        public static Shader Pipe;
        public static Shader Fade;

        private Shader(string vertFile, string fragFile)
        {
            _ID = ShaderUtil.Load(vertFile, fragFile);
            _LocationCache = new Dictionary<string, int>();
        }

        public static void LoadAll()
        {
            Background = new Shader("shaders/main.vert", "shaders/main.frag");
            Bird = new Shader("shaders/bird.vert", "shaders/bird.frag");
            Pipe = new Shader("shaders/pipe.vert", "shaders/pipe.frag");
            Fade = new Shader("shaders/fade.vert", "shaders/fade.frag");
        }

        private int GetUniformLocation(string name)
        {
            if(_LocationCache.ContainsKey(name)) return _LocationCache[name];

            int result = Gl.GetUniformLocation(_ID, name);
            if(result == -1) return result;
            else _LocationCache.Add(name, result);

            return result;
        }

        public void SetInt(string name, int value)
        {
            if(!_Enabled) Bind();
            Gl.Uniform1i(GetUniformLocation(name), value);
        }

        public void SetFloat(string name, float value)
        {
            if(!_Enabled) Bind();
            Gl.Uniform1f(GetUniformLocation(name), value);
        }

        public void SetFloat2(string name, float v1, float v2)
        {
            if(!_Enabled) Bind();
            Gl.Uniform2f(GetUniformLocation(name), v1, v2);
        }

        public void SetMatrix4f(string name, Matrix4f matrix)
        {
            if(!_Enabled) Bind();
            Gl.UniformMatrix4fv(GetUniformLocation(name), 1, false, matrix);
        }

        public void Bind()
        {
            Gl.UseProgram(_ID);
            _Enabled = true;
        }

        public void Unbind()
        {
            Gl.UseProgram(0);
            _Enabled = false;
        }

        public void Dispose()
        {
            Gl.DeleteProgram(_ID);
        }

        internal static void DisposeAll()
        {
            Background.Dispose();
        }
    }
}
