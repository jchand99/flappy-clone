using System;
using Magma.Maths;
using Magma.OpenGL;
using Magma.OpenGL.GLFW;

namespace TestMe
{
    public class Level
    {
        private VertexArray _VertexArray;
        private VertexArray _FadeVertexArray;
        private Matrix4f _Ortho;
        private int _XScroll;
        private float _Speed;
        private int _Map;

        private Bird _Bird;
        private Pipe[] _Pipes = new Pipe[5 * 2];
        private int _Index = 0;
        private float _Offset = 5.0f;
        private float _PipeYOffset = -1.5f;
        private bool _PlayerControl = true;
        private float _Time = 0;

        private Random _Random;

        private float[] vertices =
        {
            -10.0f, -10.0f * 9.0f / 16.0f, 0.0f,
            -10.0f,  10.0f * 9.0f / 16.0f, 0.0f,
              0.0f,  10.0f * 9.0f / 16.0f, 0.0f,
              0.0f, -10.0f * 9.0f / 16.0f, 0.0f
        };

        private uint[] indices =
        {
            0, 1, 2,  // first Triangle
            2, 3, 0   // second Triangle
        };

        private float[] texCoords = {
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f
        };

        public Level()
        {
            _Random = new Random();
            _XScroll = 0;
            _Map = 0;
            _Speed = 0.02f;

            _VertexArray = new VertexArray(vertices, indices, texCoords);
            _FadeVertexArray = new VertexArray(6);

            Shader.LoadAll();
            Texture.LoadAll();

            _Bird = new Bird();

            Gl.ActiveTexture(0x84C1);
            _Ortho = Matrix4f.Orthographic(-10.0f, 10.0f, 10.0f * 9.0f / 16.0f, -10.0f * 9.0f / 16.0f, -1.0f, 1.0f);
            Shader.Background.SetMatrix4f("pr_matrix", _Ortho);
            Shader.Background.SetInt("tex", 1);

            Shader.Bird.SetMatrix4f("pr_matrix", _Ortho);
            Shader.Bird.SetInt("tex", 1);

            Shader.Pipe.SetMatrix4f("pr_matrix", _Ortho);
            Shader.Pipe.SetInt("tex", 1);

            CreatePipes();
        }

        private void CreatePipes()
        {
            Pipe.Create();
            for(int i = 0; i < _Pipes.Length; i+= 2)
            {
                _Pipes[i] = new Pipe(_Offset + _Index * 3.0f, (float) _Random.NextDouble() * 5.0f + _PipeYOffset);
                _Pipes[i + 1] = new Pipe(_Pipes[i].Position.X, _Pipes[i].Position.Y - 11.5f);
                _Index += 2;
            }
        }

        private void UpdatePipes()
        {
            int index = _Index % 10;
            _Pipes[index] = new Pipe(_Offset + _Index * 3.0f, (float) _Random.NextDouble() * 5.0f + _PipeYOffset);
            _Pipes[(_Index + 1) % 10] = new Pipe(_Pipes[index].Position.X, _Pipes[index].Position.Y - 11.5f);
            _Index += 2;
        }

        public void Update(double deltaTime)
        {
            if(_PlayerControl)
            {
                _XScroll--;
                if(-_XScroll % 500 == 0) _Map++;
                if(-_XScroll > 475 && _XScroll % 200 == 0) UpdatePipes();
            }

            if(Input.Keys[(int) KeyCode.Space] && _PlayerControl)
                _Bird.Update(deltaTime, true);
            else
                _Bird.Update(deltaTime, false);

            if(_PlayerControl && Collision())
            {
                _Bird.Fall();
                _PlayerControl = false;
            }

            _Time += 0.01f;
        }

        private void RenderPipes()
        {
            Shader.Pipe.Bind();
            Shader.Pipe.SetFloat2("bird", _Bird.GetX(), _Bird.GetY());
            Shader.Pipe.SetMatrix4f("vw_matrix", Matrix4f.Translate(new Vector3f(_XScroll * (_Speed + 0.01f), 0.0f, 0.0f)));

            Texture.Pipe.Bind();
            Pipe.Mesh.Bind();
            Pipe.Mesh.Draw();
            for(int i = 0; i < _Pipes.Length; i++)
            {
                Shader.Pipe.SetMatrix4f("ml_matrix", _Pipes[i].Model);
                Shader.Pipe.SetInt("top", i % 2 != 0 ? 1 : 0);
                Pipe.Mesh.Draw();
            }
            Pipe.Mesh.Unbind();
            Texture.Pipe.Unbind();
            Shader.Pipe.Unbind();
        }

        private bool Collision()
        {
            for(int i = 0; i < _Pipes.Length; i++)
            {
                float bp = -_XScroll * (_Speed + 0.01f);
                bool collisionX = bp + Bird.SIZE / 2.0f >= _Pipes[i].Position.X &&
                                  _Pipes[i].Position.X + Pipe.Width >= bp;

                bool collisionY = _Bird.GetY() + Bird.SIZE / 2.0f >= _Pipes[i].Position.Y &&
                                  _Pipes[i].Position.Y + Pipe.Height >= _Bird.GetY();

                if(collisionX && collisionY)
                {
                    return true;
                }
            }
            return false;
        }

        public void Render()
        {
            Texture.BG.Bind();
            Shader.Background.Bind();
            Shader.Background.SetFloat2("bird", _Bird.GetX(), _Bird.GetY());
            _VertexArray.Bind();
            for(int i = _Map; i < _Map + 3; i++)
            {
                Shader.Background.SetMatrix4f("vw_matrix", Matrix4f.Translate(new Vector3f(i * 10 + _XScroll * _Speed, 0.0f, 0.0f)));
                _VertexArray.Draw();
            }
            Shader.Background.Unbind();
            Texture.BG.Unbind();

            RenderPipes();
            _Bird.Render();

            Shader.Fade.Bind();
            Shader.Fade.SetFloat("time", _Time);
            _FadeVertexArray.Render();
            Shader.Fade.Unbind();
        }

        public void Dispose()
        {
            Texture.DisposeAll();
            Shader.DisposeAll();
            _Bird.Dispose();

            for(int i = 0; i < _Pipes.Length; i++)
            {
                _Pipes[i].Dispose();
            }
            _VertexArray.Dispose();
            Glfw.Terminate();
        }
    }
}
