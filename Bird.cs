using System;
using Magma.Maths;
using Magma.OpenGL.GLFW;

namespace TestMe
{
    public class Bird
    {
        public const float SIZE = 1.0f;
        private VertexArray _Mesh;

        private Vector3f _Position;
        private float _Rotation;
        private float _Delta;

        private float[] vertices =
        {
            -SIZE / 2.0f, -SIZE / 2.0f, 0.2f,
            -SIZE / 2.0f,  SIZE / 2.0f, 0.2f,
             SIZE / 2.0f,  SIZE / 2.0f, 0.2f,
             SIZE / 2.0f, -SIZE / 2.0f, 0.2f,
        };

        private uint[] indices =
        {
            0, 1, 2,  // first Triangle
            2, 3, 0   // second Triangle
        };

        private float[] texCoords = {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f
        };

        public Bird()
        {
            _Delta = -0.05f;
            _Mesh = new VertexArray(vertices, indices, texCoords);
            _Position = new Vector3f();
        }

        public float GetY() => _Position.Y;
        public float GetX() => _Position.X;

        public void Update(double deltaTime, bool flap)
        {
            _Position.Y -= _Delta;
            if(flap)
                _Delta = 0.05f;
            else
                _Delta -= 0.001f;

            _Rotation = -_Delta * 90.0f;

        }

        public void Fall()
        {
            _Delta = -0.05f;
        }

        public void Render()
        {
            Shader.Bird.Bind();
            Shader.Bird.SetMatrix4f("ml_matrix", Matrix4f.Translate(_Position) * Matrix4f.Rotate(((_Rotation * MathF.PI) / 180.0f) * 2.0f, new Vector3f(0.0f, 0.0f, 1.0f)));
            Texture.Bird.Bind();
            _Mesh.Render();
            Texture.Bird.Unbind();
            Shader.Bird.Unbind();
        }

        public void Dispose()
        {
            _Mesh.Dispose();
        }

        internal float GetSize()
        {
            throw new NotImplementedException();
        }
    }
}
