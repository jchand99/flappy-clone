using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Magma.OpenGL;

namespace TestMe
{
    public class Texture
    {
        private byte[] _Pixels;
        private int _Height;
        private int _Width;
        private Rectangle _ImageRectangle;
        private uint _ID;
        private Magma.OpenGL.PixelFormat _Format;
        private PixelInternalFormat _InternalFormat;

        public static Texture BG;
        public static Texture Bird;
        public static Texture Pipe;

        private Texture(string assetFile)
        {
            Bitmap bitmap = default;
            try
            {
                bitmap = new Bitmap(assetFile);
            }
            catch(FileNotFoundException e)
            {
                Console.WriteLine(e.StackTrace);
            }

            _Width = bitmap.Width;
            _Height = bitmap.Height;

            _ImageRectangle = new Rectangle(0, 0, _Width, _Height);

            if(assetFile.Contains(".jpg"))
            {
                _InternalFormat = PixelInternalFormat.Rgb;
                _Format = Magma.OpenGL.PixelFormat.Bgr;
            }
            else
            {
                _InternalFormat = PixelInternalFormat.Rgba;
                _Format = Magma.OpenGL.PixelFormat.Bgra;
            }

            LoadData(bitmap);
            CreateTexture();
        }

        public static void LoadAll()
        {
            BG = new Texture("assets/bg.jpg");
            Bird = new Texture("assets/bird.png");
            Pipe = new Texture("assets/pipe.png");
        }

        public static void DisposeAll()
        {
            BG.Dispose();
        }

        public void Dispose()
        {
            Gl.DeleteTextures(1, new uint[] {_ID});
        }

        public void Bind()
        {
            Gl.BindTexture(TextureTarget.Texture2D, _ID);
        }

        public void Unbind()
        {
            Gl.BindTexture(TextureTarget.Texture2D, 0);
        }

        private unsafe void LoadData(Bitmap bitmap)
        {
            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
            BitmapData bitmapData = bitmap.LockBits(_ImageRectangle, ImageLockMode.ReadOnly, bitmap.PixelFormat);
            int length = Math.Abs(bitmapData.Stride) * bitmapData.Height;

            _Pixels = new byte[length];
            fixed(byte* ptr = _Pixels)
            Buffer.MemoryCopy((void*) bitmapData.Scan0, ptr, length, length);


            bitmap.UnlockBits(bitmapData);
        }

        private void CreateTexture()
        {
            uint[] id;
            Gl.GenTextures(1, out id);
            _ID = id[0];

            Gl.BindTexture(TextureTarget.Texture2D, _ID);
            Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Nearest);
            Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Nearest);

            Gl.TexImage2D(TextureTarget.Texture2D, 0, _InternalFormat, _Width, _Height, 0, _Format, PixelType.UnsignedByte, _Pixels);
            Gl.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        }
    }
}
