using System;
using Magma.OpenGL.GLFW;

namespace TestMe
{
    public static class Input
    {

        public static bool[] Keys;

        static Input()
        {
            Keys = new bool[65536];
        }

        public static void SetCallback(IntPtr window)
        {
            unsafe
            {
                Glfw.SetKeyCallback(window, &KeyCallback);
            }
        }

        private static void KeyCallback(IntPtr window, KeyCode key, int scancode, ButtonState action, ModifierKey mod)
        {
            Keys[(int) key] = action != ButtonState.Release;
        }

        public static bool IsKeyPressed(KeyCode key)
        {
            return Keys[(int) key];
        }
    }
}
